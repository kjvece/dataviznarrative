window.addEventListener('load', () => {
    const root = document.getElementById('root')
    let state = {
        page: 1,
        maxPage: 3,
        usOnly: false,
        listeners: {},
        addEventListener: function (type, callback) {
            listener = this.listeners[type] || []
            listener = [...listener, callback]
            this.listeners[type] = listener
        },
        dispatchEvent: function (type, oldState) {
            this.listeners[type]?.forEach(listener => {
                listener(oldState)
            })
        },
        updateState: function (newState) {
            console.log('newState', newState)
            let oldState = {...this}
            for (let attr in newState)
                this[attr] = newState[attr]
            console.log('new state', this)
            console.log('old state', oldState)
            this.dispatchEvent('change', oldState)
        },
    }

    const prev = document.getElementById('btn-prev')
    prev.addEventListener('click', () => {
        state.updateState({
            page: Math.max(1, state.page - 1),
        })
    })
    state.addEventListener('change', () => {
        if (state.page <= 1)
            prev.style.display = 'none'
        else
            prev.style.display = 'inline-flex'
    })

    const next = document.getElementById('btn-next')
    next.addEventListener('click', () => {
        state.updateState({
            page: Math.min(state.maxPage, state.page + 1),
        })
    })
    state.addEventListener('change', () => {
        if (state.page >= state.maxPage)
            next.style.display = 'none'
        else
            next.style.display = 'inline-flex'
    })

    document.addEventListener('keydown', (e) => {
        e = e || window
        if (e.keyCode === 37)
            // left arrow
            state.updateState({ page: Math.max(1, state.page-1) })
        if (e.keyCode === 39)
            // right arrow
            state.updateState({ page: Math.min(state.maxPage, state.page+1) })
    })

    const breadcrumb = document.getElementById('breadcrumb')
    state.addEventListener('change', () => {
        breadcrumb.innerHTML = ''
        for (let i = 1; i <= state.maxPage; i++) {
            let span = document.createElement('span')
            span.innerHTML = '.'
            span.classList.add('crumb')
            span.style.zIndex = 2;
            span.addEventListener('click', ((i) => function() {
                state.updateState({ page: i })
            })(i))
            if (i === state.page)
                span.classList.add('active')
            breadcrumb.appendChild(span)

        }
    })
    
    const pages = document.querySelectorAll('.page')

    const content = document.getElementById('content')
    content.style.width = `${(pages.length || 1) * 100}vw`

    state.addEventListener('change', () => {
        content.style.left = `${100 * (1-state.page)}vw`
    })

    const clickArrow = document.getElementById('click-section')
    const clickText = document.getElementById('click-text')
    let bounceI = 0;
    let bounceDirection = 2;
    function bounceAnimation() {
        clickArrow.style.paddingLeft = `${bounceI}px`
        bounceI += bounceDirection
        if (bounceI > 15)
            bounceDirection = -1
        if (bounceI < 0)
            bounceDirection = 2
        if (state.page == 1)
            requestAnimationFrame(bounceAnimation)
    }
    state.addEventListener('change', () => requestAnimationFrame(bounceAnimation))

    var data = [];
    d3.csv('data/2021NewCoderSurvey.csv', (d) => {
        data.push(d)
        document.getElementById('click-text').innerText = 'click the arrow to begin '
    })

    Object.filter = (obj, test) => Object.fromEntries(Object.entries(obj).filter(test))
    Object.toArray = (obj) => Object.entries(obj).map(([key, value]) => ({key:key, value:value}))
    const brightPalette = ['#ffbe0b', '#fb5607', '#ff006e', '#8338ec', '#3a86ff']
    const longPalette = ['#001219', '#005f73', '#0a9396', '#94d2bd', '#e9d8a6', '#339b00', '#ca6702', '#bb3e03', '#ae2012', '#9b2226']

    const numPeople = document.getElementById('num-people')
    const gender = document.getElementById('gender')
    const age = document.getElementById('age')
    state.addEventListener('change', (oldState) => {
        if (state.page === 2 && oldState.page !== 2) {

            // number of people
            let i = 0
            function animatePeople() {
                // delay by half a second
                if (i++ < 30) return requestAnimationFrame(animatePeople)
                const len = data.length
                const interval = parseInt(len / 90)
                const newVal = Math.min(parseInt(numPeople.innerText) + interval, len)
                numPeople.innerText = newVal
                if (newVal < len)
                    requestAnimationFrame(animatePeople)
            }
            requestAnimationFrame(animatePeople)

            // gender breakdown
            let genderFreqs = data.map(d => d['24. Which of the following best represents how you think of yourself?'].trim())
                .filter(d => d.length > 0)
                .reduce((p, c) => {
                    p[c] = (p[c] || 0) + 1
                    return p
                }, {})
            genderFreqs = Object.toArray(genderFreqs).filter(({ value }) => value > 10)
            const maxGender = genderFreqs.reduce((p, c) => Math.max(p, c.value), 0)
            const genderBars = d3.select('#gender')
                .selectAll('rect')
                .data(genderFreqs)
                .enter()
                .append('rect')
                .attr('width', .75*(100/genderFreqs.length))
                .attr('x', (d,i) => (100/genderFreqs.length)*i)
                .attr('y', 30)
                .style('fill', (_,i) => brightPalette[(i+4)%brightPalette.length])
                .on('mousemove', e => {
                    d3.select('#tool-tip')
                        .style('display', 'initial')
                        .style('left', e.pageX+10)
                        .style('top', e.pageY)
                        .text(`${e.srcElement.__data__.value} people`)
                })
                .on('mouseout', e => {
                    d3.select('#tool-tip')
                        .style('display', 'none')
                })
                .transition()
                .delay(500)
                .duration(1700)
                .attr('height', d => d.value * 60 / maxGender )
            const genderLabels = d3.select('#gender')
                .selectAll('text')
                .data(genderFreqs)
                .enter()
                .append('text')
                .attr('transform', 'scale(1,-1) rotate(90)')
                .attr('x', -28)
                .attr('y', (d,i) => -(100/genderFreqs.length)*(i+.3))
                .style('fill', 'white')
                .style('font-size', '.3em')
                .text(d => d.key)

            // age breakdown
            const dataAges = data.map(d => d['23. How old are you?']).filter(d => d.trim().length > 0)
            const dataAgeBins = d3.histogram().thresholds(9)(dataAges).map(d => d.length)
            const dataMaxAge = dataAgeBins.reduce((p,c) => Math.max(p,c), 0)
            d3.select('#age')
                .selectAll('rect')
                .data(dataAgeBins)
                .enter()
                .append('rect')
                .attr('fill', (_,i) => brightPalette[(i+3) % brightPalette.length])
                .attr('width', 10)
                .attr('x', (_,i) => 10*i)
                .attr('y', 80)
                .on('mousemove', e => {
                    d3.select('#tool-tip')
                        .style('display', 'initial')
                        .style('left', e.pageX+10)
                        .style('top', e.pageY)
                        .text(`${e.srcElement.__data__} people`)
                })
                .on('mouseout', e => {
                    d3.select('#tool-tip')
                        .style('display', 'none')
                })
                .transition()
                .delay(500)
                .duration(1700)
                .attr('height', d => 80*d/dataMaxAge)
                .attr('y', d => 80 - 80*d/dataMaxAge)
            d3.select('#age')
                .selectAll('text')
                .data(dataAgeBins)
                .enter()
                .append('text')
                .attr('x', (_,i) => 10*i+2)
                .attr('y', 86)
                .style('fill', 'white')
                .style('font-size', '.25em')
                .text((_,i) => `${(i+1)*10}s`)
        }
    })

    const toggleAll = document.getElementById('toggle-all')
    const toggleUs = document.getElementById('toggle-us')
    toggleAll.addEventListener('click', () => {
        state.updateState({ usOnly: false })
        toggleAll.classList.add('toggle-active')
        toggleUs.classList.remove('toggle-active')
    })
    toggleUs.addEventListener('click', () => {
        state.updateState({ usOnly: true })
        toggleUs.classList.add('toggle-active')
        toggleAll.classList.remove('toggle-active')
    })

    state.addEventListener('change', (oldState) => {
        if (state.page === 4 && (oldState.page !== 4 || oldState.usOnly != state.usOnly)) {
            const hoursPerWeek = data.map(d => {
                const hours = d['7. About how many hours do you spend learning each week?']
                const salary = d["22. About how much money did you earn last year from any job or employment (in US Dollars)? "]
                const us = d["27. If you are living in the US, which state do you currently live in? "]
                return { hours, salary, us }
            }).filter( ({ hours, salary }) => {
                if (hours === undefined || salary === undefined) return false
                if (hours.trim().length == 0 || salary.trim().length == 0) return false
                //if (salary === "None") return false
                return true
            }).map( ({ hours, salary, us }) => {
                hours = parseInt(hours)
                salary = salary
                    .replace(/[^0-9 ]/g, '')
                    .trim()
                us = us.trim().length > 0
                return { hours, salary, us }
            }).filter( ({ hours, salary, us }) => {
                return salary.trim().length > 0
            }).map( ({ hours, salary, us }) => {
                const range = salary.trim().split(/\s+/)
                const avg = range.reduce((a,b) => a + parseInt(b), 0) / range.length
                return { hours, salary: avg, us }
            })
            const hoursPerWeekUsOnly = hoursPerWeek.filter(d => d.us)
            const toCounts = function (p, { hours, salary }) {
                if (p[hours] === undefined) p[hours] = {}
                p[hours][salary] = (p[hours][salary] || 0) + 1
                return p
            }
            const toCountsArray = function(counts) {
                const results = []
                for (const [hour, salaries] of Object.entries(counts)) {
                    for (const [salary, count] of Object.entries(salaries)) {
                        results.push({
                            hour,
                            salary,
                            count,
                        })
                    }
                }
                return results
            }
            const hoursPerWeekCounts = toCountsArray(hoursPerWeek.reduce(toCounts, {}))
            const hoursPerWeekCountsUsOnly = toCountsArray(hoursPerWeekUsOnly.reduce(toCounts, {}))
            const margin = 25;
            const getScales = function(hours) {
                let maxX = 0;
                let maxY = 0;
                let maxCount = 0;
                for (const {hour, salary, count} of hours) {
                    maxX = Math.max(hour, maxX)
                    maxY = Math.max(salary, maxY)
                    maxCount = Math.max(count, maxCount)
                }
                const x = d3.scaleLinear().domain([0, maxX]).range([margin, 100-margin])
                const y = d3.scaleLinear().domain([0, maxY]).range([100-margin, margin])
                return { maxX, maxY, maxCount, x, y }
            }
            const hpwCounts = state.usOnly ? hoursPerWeekCountsUsOnly : hoursPerWeekCounts
            console.log("SHOWING US DATA:", state.usOnly)
            const scale = getScales(hpwCounts)
            d3.select('#study-vs-salary').html('')
            d3.select('#study-vs-salary')
                .append('g')
                .selectAll('circle')
                .data(hpwCounts)
                .enter()
                .append('circle')
                .attr('cx', d => margin + (100-2*margin)*(d.hour / scale.maxX))
                .attr('cy', d => margin + (100-2*margin)*((scale.maxY - d.salary)/scale.maxY))
                .attr('r', d => Math.max(2*(d.count / scale.maxCount), .25))
                .style('fill', 'white')
            d3.select('#study-vs-salary')
                .append('g')
                .attr('transform', `translate(${margin/1.5},0)`)
                .style('font-size', 3)
                .call(d3.axisLeft(scale.y))
            d3.select('#study-vs-salary')
                .append('g')
                .attr('transform', `translate(${0},${103-margin})`)
                .style('font-size', 2)
                .call(d3.axisBottom(scale.x))
            const annotations = [
                {
                    note: {
                        label: "more self study doesn't appear to directly relate to a higher salary",
                    },
                    type: d3.annotationCalloutCircle,
                    x: margin*1.25,
                    y: margin*1.25,
                    dy: -10,
                    dx: 10,
                    subject: { radius: 10, radiusPadding: 1 },
                },
            ]
            const makeAnnotations = d3.annotation().annotations(annotations)
            d3.select('#study-vs-salary')
                .append('g')
                .attr('class', 'annotation-group')
                .style('font-size', '.3em')
                .call(makeAnnotations)
            d3.select('#study-vs-salary')
                .append('g')
                .append('text')
                .attr('x', margin)
                .attr('y', 95)
                .style('font-size', '.3em')
                .attr('fill', 'white')
                .text('hours studied per week')
            d3.select('#study-vs-salary')
                .append('g')
                .attr('transform', 'rotate(-90)')
                .append('text')
                .attr('x', -65)
                .attr('y', -10)
                .style('font-size', '.3em')
                .attr('fill', 'white')
                .text('salary in USD')
        }
    })


    state.addEventListener('change', (oldState) => {
        if (state.page === 3 && (oldState.page !== 3 || oldState.usOnly != state.usOnly)) {
            const reasonCounts = data.map(d => d['1. What is your biggest reason for learning to code?'])
                .reduce((p,c) => {
                    p[c] = (p[c] || 0) + 1
                    return p
                }, {})
            const reasons = []
            for (const [reason, count] of Object.entries(reasonCounts)) {
                reasons.push({ reason, count})
            }
            const filteredReasons = reasons.filter(r => r.reason != '' && r.count > 10)
            console.log(filteredReasons)
            const counts = filteredReasons.map(d => d.count)
            console.log(counts)

            d3.select('#motivation')
                .append('g')
                .attr('transform', 'translate(50,25)')
                .selectAll('path')
                .data(d3.pie()(counts))
                .enter()
                .append('path')
                .attr('d', d3.arc()
                    .innerRadius(0)
                    .outerRadius(15))
                .attr('fill', (_,i) => longPalette[(i+2) % longPalette.length])
                .on('mousemove', e => {
                    const eCount = e.srcElement.__data__.value
                    const eReason = filteredReasons.filter(r => r.count === eCount)[0].reason
                    d3.select('#tool-tip')
                        .style('display', 'initial')
                        .style('left', e.pageX+10)
                        .style('top', e.pageY)
                        .text(eReason)
                })
                .on('mouseout', e => {
                    d3.select('#tool-tip')
                        .style('display', 'none')
                })
            d3.select('#motivation')
                .append('g')
                .append('path')
                .attr('d', 'M 65 10 h 5 v 30 h -5 h 5 v -15 h 5')
                .attr('stroke', 'white')
                .attr('fill', 'transparent')
            d3.select('#motivation')
                .append('g')
                .attr('transform', 'rotate(90)')
                .append('text')
                .text('more than half want to start new careers')
                .attr('x', 5)
                .attr('y', -78)
                .attr('fill', 'white')
                .style('font-size', '.125em')
            
            
        }
    })

    state.updateState({ maxPage: pages.length || 1 })
})
